FROM python:3
WORKDIR /usr/src/app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
ENV HELLOSERVER_LISTEN_PORT=5051
CMD [ "python", "./server.py" ]
