# HelloServer

Hello World containerized python-based web service.

## Prerequisites

1. [Docker](https://docs.docker.com/get-docker/)

## Commands

Use `docker-compose`, or `docker` directly:

| Task             | Command                                     |
|------------------|---------------------------------------------|
| Build            | `docker build -t helloserver .`             |
| Run and watch    | `docker run -it -p 5051:5051 helloserver`   |
| Run as a daemon  | `docker run -d -p 5051:5051 helloserver`    |

## Resources

* [more authoritative and detailed description of these same concepts](https://www.docker.com/blog/containerized-python-development-part-1/)
* [python official image page on Docker Hub](https://hub.docker.com/_/python)
* [python 3.8 `time.strftime()` library reference](https://docs.python.org/3.8/library/time.html#time.strftime)
* [docker-compose v3 reference](https://docs.docker.com/compose/compose-file/compose-file-v3/)

## Troubleshooting

Building on a Raspberry Pi may fail with `Fatal Python error: init_interp_main: can't initialize time`. Patches welcome. No workaround exists at this time.

## Copyleft and License

* Copyright ©2021 Adam Monsen &lt;haircut@gmail.com&gt;
* License: AGPL v3 or later (see COPYING)
