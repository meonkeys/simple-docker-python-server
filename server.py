#!/usr/bin/python3

from flask import Flask
import os
import time

server = Flask(__name__)

@server.route("/")
def hello():
    localTime = time.strftime("%H:%M:%S %Z", time.gmtime())
    return "🏜️ Hello World! It is now: {}".format(localTime)

if __name__ == "__main__":
    print('💃 Starting server')
    server.run(host='0.0.0.0', port=os.environ['HELLOSERVER_LISTEN_PORT'])
